import { useState } from "react";
import { Link } from "react-router-dom";

import styles from "../../styles/Main.module.css";
import {FIELDS} from "./home.consts";

export const Home = () => {

    const [name, setName] = useState('');
    const [room, setRoom] = useState('');

    const changeNameHandler = (e) => setName(e.target.value);
    const changeRoomHandler = (e) => setRoom(e.target.value);

    const joinRoomHandler = (e) => {
        const isDisabled = !name || !room;
        if (isDisabled) e.preventDefault();
    }

    console.log('name:room', name, room);

    return (
        <div className={styles.wrap}>
            <div className={styles.container}>
                <h1 className={styles.heading}>Присоединится к комнате</h1>

                <form className={styles.form}>
                    <div className={styles.group}>
                        <input
                            type="text"
                            name={FIELDS.NAME}
                            value={name}
                            placeholder="Ваше имя"
                            className={styles.input}
                            onChange={changeNameHandler}
                            autoComplete="off"
                            required
                        />
                    </div>
                    <div className={styles.group}>
                        <input
                            type="text"
                            name={FIELDS.ROOM}
                            placeholder="Комната"
                            value={room}
                            className={styles.input}
                            onChange={changeRoomHandler}
                            autoComplete="off"
                            required
                        />
                    </div>

                    <Link
                        className={styles.group}
                        onClick={joinRoomHandler}
                        to={`/chat/${name}/${room}`}
                    >
                        <button type="submit" className={styles.button}>
                            Войти
                        </button>
                    </Link>
                </form>
            </div>
        </div>
    );
};