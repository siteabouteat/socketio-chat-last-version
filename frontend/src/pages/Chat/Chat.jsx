import {memo, useEffect, useState} from "react";
import { useNavigate, useParams } from "react-router-dom";
import EmojiPicker from "emoji-picker-react";

import socket from "../../socket";

import icon from "../../images/emoji.svg";
import styles from "../../styles/Chat.module.css";
import {Messages} from "./components";

export const Chat = memo(() => {
    const params = useParams();
    const navigate = useNavigate();

    const [isOpen, setIsOpen] = useState(false);
    const [isConnect, setIsConnect] = useState(false);

    const [message, setMessage] = useState('');
    const [messages, setMessages] = useState([]);
    const [users, setUsers] = useState(0);

    console.log('messages: ', messages);
    console.log('isConnect: ', isConnect);

    useEffect(() => {
        socket.emit('join', params);
        return () => {
            socket.off('join');
        };
    }, []);

    useEffect(() => {
        socket.on('message', async ({ data }) => {
            await setMessages((prevMessages) => [...prevMessages, data]);

        });
        return () => {
            socket.off('message');
        };
    }, []);

    useEffect(() => {
        socket.on('cntRoomUser', ({ data: { users } }) => {
            setUsers(users);
        });
        return () => {
            socket.off('cntRoomUser');
        };
    }, []);

    const openImojiHandler = () => setIsOpen(!isOpen);

    const changeMessageHandler = (e) => setMessage(e.target.value);

    const onEmojiClickHandler = ({ emoji }) => setMessage(`${message} ${emoji}`);

    const addNewMessageHandler = (e) => {
        e.preventDefault();

        if(!message) return;

        socket.emit('sendMessage', { message, params });

        setMessage('');
    };

    const leftRoomHandler = (e) => {
        e.preventDefault();
        socket.emit('leftRoom', { params });
        navigate("/");
    };

    return (
        <div className={styles.wrap}>
            <div className={styles.header}>
                <div className={styles.title}>Комната: {params.room}</div>
                <div className={styles.users}>{users} пользователей в комнате</div>
                <button className={styles.left} onClick={leftRoomHandler}>
                    Покинуть комнату
                </button>
            </div>

            <div className={styles.messages}>
                <Messages messages={messages} name={params.name} />
            </div>

            <form className={styles.form} onSubmit={addNewMessageHandler}>
                <div className={styles.input}>
                    <input
                        type="text"
                        name="message"
                        placeholder="Напишите сообщение"
                        value={message}
                        onChange={changeMessageHandler}
                        autoComplete="off"
                        required
                    />
                </div>
                <div className={styles.emoji}>
                    <img src={icon} alt="" onClick={openImojiHandler} />

                    {isOpen && (
                        <div className={styles.emojies}>
                            <EmojiPicker onEmojiClick={onEmojiClickHandler} />
                        </div>
                    )}
                </div>

                <div className={styles.button}>
                    <input type="submit" onSubmit={addNewMessageHandler} value="Отправить" />
                </div>
            </form>
        </div>
    );
});