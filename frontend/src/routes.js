import React from 'react';
import { Routes, Route } from 'react-router-dom';
import {Home, Chat} from "./pages";


export const AppRoutes = () => {
    return (
        <>
            <Routes>
                <Route path="/" exact element={
                    <>
                        <Home />
                    </>
                } />
                <Route path="/chat/:name/:room" element={<Chat />} />
            </Routes>
        </>
    )
}