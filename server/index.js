const path = require('path');
const dotenv = require('dotenv');
const cors = require('cors');
const logger = require('morgan');
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');

const { addUser, findUser, getAllUserByRoom, removeUser } = require("./src/data/users");

const indexRoutes = require('./src/routes/index.route');

dotenv.config({
    path: path.resolve(__dirname, '.env'),
});

const PORT = process.env.PORT || 3078;

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
    extended: true,
}));
app.use(cookieParser());

app.use('/api', indexRoutes);

const server = require('http').createServer(app);
const io = require('socket.io')(server);

io.on('connection', (socket) => {

    socket.on('join', ({name, room}) => {

        socket.join(room);

        const { user } = addUser({name, room});

        socket.emit('message', {
            data: { user: { name: 'Admin'}, message: `Привет, ${user.name}` }
        });

        socket.broadcast.to(user.room).emit('message', {
            data: { user: { name: 'Admin'},  message: `Пользователь ${user.name} присоединился` }
        });

        io.to(room).emit('cntRoomUser', { data: { room: user.room, users: getAllUserByRoom(user.room) } });
    });

    socket.on('sendMessage', ({ message, params }) => {
        const user = findUser(params);
        if(user){
            io.to(user.room).emit('message', { data: { user, message } });
        }
    });

    socket.on('leftRoom', ({ message, params }) => {
        const user = removeUser(params);
        if(user){
            io.to(user.room).emit('message', { data: { user: { name: 'Admin'},  message: `Пользователь ${user.name} покинул комнату` } });
            io.to(user.room).emit('message', { data: { user, message } });
        }
    });

    io.on('disconnect', () => console.log('socket disconnected...'))
});


server.listen(PORT, () => {
    console.log(`Server started on http://localhost:${PORT}`);
    mongoose.connect(
        process.env.MONGO_URL, {
            useNewUrlParser: true,
        },
    ).then(() => {
        console.log('Connection mongodb success!!!');
    }).catch((error) => console.log(error));
});