const route = require('express');
const IndexController = require("../controllers/index.controller");

class IndexRoute extends IndexController {

    router;

    constructor() {
        super();
        this.router = route.Router();
        this.routes();
    }

    routes() {
        this.router.get('/', this.index);
    }

}

module.exports = new IndexRoute().router;