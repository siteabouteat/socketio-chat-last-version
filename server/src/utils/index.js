const trimStr = str => str && str.trim().toLowerCase();

module.exports = {
    trimStr,
};